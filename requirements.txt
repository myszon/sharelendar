setuptools
pip>=7
pytz
Django==1.8.5
django-grappelli
django-extensions
django-timezone-field
django-html5-colorfield
