# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('calendar', '0001_initial'),
        ('user', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='calendaruser',
            name='default_calendar',
            field=models.ForeignKey(editable=False, to='calendar.Calendar', null=True),
        ),
    ]
