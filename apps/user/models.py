# -*- coding: utf-8 -*-
# Author Szymon Nowicki <szymon.nowicki@agitive.com>
# (C) Agitive sp. z o. o. 2015
# created at: 08/10/15 19:55

from __future__ import unicode_literals
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from timezone_field.fields import TimeZoneField
from django.db import models


class CalendarUser(AbstractUser):
    """
    A user with additional data needed for calendars.
    default_calendar is nullable to allow it to be added in a signal,
    the calendar itself cannot be created without a user.
    """
    timezone = TimeZoneField(default=settings.TIME_ZONE)
    default_calendar = models.ForeignKey('calendar.Calendar', null=True, editable=False)
