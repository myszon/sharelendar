# -*- coding: utf-8 -*-
# Author Szymon Nowicki <szymon.nowicki@agitive.com>
# (C) Agitive sp. z o. o. 2015
# created at: 11/10/15 20:41

from __future__ import unicode_literals

from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.calendar.models import Calendar
from apps.user.models import CalendarUser


@receiver(post_save, sender=CalendarUser)
def create_default_calendar(instance, raw, **kwargs):
    """
    Ensures each user has a default calendar
    """
    if not raw and instance.default_calendar is None:
        calendar = Calendar(owner=instance, name='Default')
        calendar.save()
        instance.default_calendar = calendar
        instance.save()
