# -*- coding: utf-8 -*-
# Author Szymon Nowicki <szymon.nowicki@agitive.com>
# (C) Agitive sp. z o. o. 2015
# created at: 08/10/15 19:55

from __future__ import unicode_literals

default_app_config = 'apps.user.app.UserAppConfig'
