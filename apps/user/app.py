# -*- coding: utf-8 -*-
# Author Szymon Nowicki <szymon.nowicki@agitive.com>
# (C) Agitive sp. z o. o. 2015
# created at: 11/10/15 20:44

from __future__ import unicode_literals
from django.apps.config import AppConfig


class UserAppConfig(AppConfig):
    name = 'apps.user'

    def ready(self):
        # noinspection PyUnresolvedReferences
        import apps.user.signals
