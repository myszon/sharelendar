# -*- coding: utf-8 -*-
# Author Szymon Nowicki <szymon.nowicki@agitive.com>
# (C) Agitive sp. z o. o. 2015
# created at: 08/10/15 20:52

from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.models import Group

from apps.user.models import CalendarUser

admin.site.register(CalendarUser)
admin.site.unregister(Group)
