# -*- coding: utf-8 -*-
# Author Szymon Nowicki <szymon.nowicki@agitive.com>
# (C) Agitive sp. z o. o. 2015
# created at: 08/10/15 19:56

from __future__ import unicode_literals

from django.contrib.auth import authenticate
from django.test import TestCase

from apps.user.models import CalendarUser


class UserTestCase(TestCase):
    def setUp(self):
        CalendarUser.objects.create_user('test', 'test@test.com', 'test')

    def test_user_login(self):
        """
        Login test case.
        """
        user = authenticate(username='test', password='test')
        self.assertIsNotNone(user)
        self.assertTrue(user.is_active)
        self.assertTrue(user.is_authenticated())

    def test_user_login_fail(self):
        """
        Login fail test case.
        """
        user = authenticate(username='test', password='tt')
        self.assertIsNone(user)

    def test_user_default_calendar(self):
        """
        Check if default calendar is created properly.
        """
        CalendarUser.objects.create_user('test1', 'test1@test.com', 'test1')
        u = CalendarUser.objects.get(username = 'test1')
        self.assertIsNotNone(u.default_calendar)
