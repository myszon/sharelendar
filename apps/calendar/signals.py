# -*- coding: utf-8 -*-
# Author Szymon Nowicki <szymon.nowicki@agitive.com>
# (C) Agitive sp. z o. o. 2015
# created at: 11/10/15 21:35

from __future__ import unicode_literals

from django.db.models.signals import pre_save
from django.dispatch import receiver

from apps.calendar.models import EventInvite, Event


@receiver(pre_save, sender=EventInvite)
def create_default_calendar(instance, raw, **kwargs):
    """
    Ensures invited user has an editable instance of the event
    """
    if not raw and instance.user is not None and instance.user.default_calendar is not None:
        event = Event.objects.get(id=instance.event_id)
        event.pk = None
        event.calendar = instance.user.default_calendar
        event.save()
        instance.user_event = event
