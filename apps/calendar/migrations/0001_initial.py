# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import colorfield.fields
from django.conf import settings
import timezone_field.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Calendar',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128)),
                ('color', colorfield.fields.ColorField(default='#00ff00', max_length=10)),
                ('owner', models.ForeignKey(related_name='calendars', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='CalendarShare',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('level', models.CharField(max_length=16, choices=[('read', 'Read'), ('write', 'Write')])),
                ('calendar', models.ForeignKey(related_name='shares', editable=False, to='calendar.Calendar')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=128)),
                ('description', models.TextField(max_length=2048)),
                ('full_day', models.BooleanField(default=False)),
                ('timezone', timezone_field.fields.TimeZoneField(default=b'UTC', null=True, blank=True)),
                ('start_year', models.IntegerField()),
                ('start_month', models.IntegerField()),
                ('start_day', models.IntegerField()),
                ('start_hour', models.IntegerField(null=True, blank=True)),
                ('start_minute', models.IntegerField(null=True, blank=True)),
                ('start_second', models.IntegerField(null=True, blank=True)),
                ('end_year', models.IntegerField()),
                ('end_month', models.IntegerField()),
                ('end_day', models.IntegerField()),
                ('end_hour', models.IntegerField(null=True, blank=True)),
                ('end_minute', models.IntegerField(null=True, blank=True)),
                ('end_second', models.IntegerField(null=True, blank=True)),
                ('calendar', models.ForeignKey(related_name='events', to='calendar.Calendar')),
            ],
        ),
        migrations.CreateModel(
            name='EventInvite',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('state', models.CharField(max_length=16, choices=[('unknown', 'Unknown'), ('maybe', 'Maybe'), ('yes', 'Yes'), ('no', 'No')])),
                ('event', models.ForeignKey(related_name='invites', editable=False, to='calendar.Event')),
                ('user', models.ForeignKey(related_name='invites', to=settings.AUTH_USER_MODEL)),
                ('user_event', models.OneToOneField(related_name='parent_event', null=True, editable=False, to='calendar.Event')),
            ],
        ),
    ]
