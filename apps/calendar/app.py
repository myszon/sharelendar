# -*- coding: utf-8 -*-
# Author Szymon Nowicki <szymon.nowicki@agitive.com>
# (C) Agitive sp. z o. o. 2015
# created at: 11/10/15 21:34

from __future__ import unicode_literals

from django.apps import AppConfig


class CalendarAppConfig(AppConfig):
    name = 'apps.calendar'

    def ready(self):
        # noinspection PyUnresolvedReferences
        import apps.calendar.signals
