# -*- coding: utf-8 -*-
# Author Szymon Nowicki <szymon.nowicki@agitive.com>
# (C) Agitive sp. z o. o. 2015
# created at: 08/10/15 20:43

from __future__ import unicode_literals

default_app_config = 'apps.calendar.app.CalendarAppConfig'
