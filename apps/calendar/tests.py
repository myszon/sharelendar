# -*- coding: utf-8 -*-
# Author Szymon Nowicki <szymon.nowicki@agitive.com>
# (C) Agitive sp. z o. o. 2015
# created at: 11/10/15 22:18

from __future__ import unicode_literals

from datetime import date, datetime

from django.test import TestCase
import pytz

from apps.calendar.models import Calendar, Event
from apps.user.models import CalendarUser


class CalendarTestCase(TestCase):
    def setUp(self):
        u1 = CalendarUser.objects.create_user('test1', 'test1@test.com', 'test')
        u2 = CalendarUser.objects.create_user('test2', 'test1@test.com', 'test')
        u3 = CalendarUser.objects.create_user('test3', 'test1@test.com', 'test')

        u1c = Calendar(owner=u1, name='calu1')
        u2c = Calendar(owner=u2, name='calu2')
        u3c = Calendar(owner=u3, name='calu3')

    def test_event_full_day(self):
        """
        Full day tests
        """
        u = CalendarUser.objects.get(username='test1')
        e = Event(calendar=u.default_calendar,
                  title='Event',
                  full_day=True)
        start = date(2015, 4, 1)
        end = date(2015, 4, 2)
        e.start = start
        e.end = end

        e.save()
        e = Event.objects.get(pk=e.id)
        self.assertEqual(start, e.start)
        self.assertEqual(end, e.end)

    def test_event_normal(self):
        """
        Full day tests
        """
        u = CalendarUser.objects.get(username='test1')
        e = Event(calendar=u.default_calendar,
                  title='Event',
                  full_day=False,
                  timezone=pytz.utc)
        start = datetime(2015, 4, 1, 10, 10, 5, tzinfo=pytz.utc)
        end = datetime(2015, 4, 2, 11, 11, 6, tzinfo=pytz.utc)
        e.start = start
        e.end = end

        e.save()
        e = Event.objects.get(pk=e.id)
        self.assertEqual(start, e.start)
        self.assertEqual(end, e.end)
