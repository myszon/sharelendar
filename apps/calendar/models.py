# -*- coding: utf-8 -*-
# Author Szymon Nowicki <szymon.nowicki@agitive.com>
# (C) Agitive sp. z o. o. 2015
# created at: 08/10/15 20:44

from __future__ import unicode_literals
import datetime

from colorfield.fields import ColorField
from django.conf import settings
from django.db import models
from django.utils import timezone
from model_utils.choices import Choices
from timezone_field.fields import TimeZoneField

from apps.user.models import CalendarUser

ACCESS_LEVELS = Choices(
    ('read', 'Read'),
    ('write', 'Write'),
)

INVITE_STATES = (
    ('unknown', 'Unknown'),
    ('maybe', 'Maybe'),
    ('yes', 'Yes'),
    ('no', 'No'),
)


class Calendar(models.Model):
    """
    A user calendar.
    """
    owner = models.ForeignKey(CalendarUser, related_name='calendars')
    name = models.CharField(max_length=128)
    color = ColorField(default='#00ff00')


class Event(models.Model):
    """
    A calendar event.
    The parts of the time touple are kept separately to allow easy jump between full day
    and normal events.
    """
    calendar = models.ForeignKey(Calendar, related_name='events')
    title = models.CharField(max_length=128)
    description = models.TextField(max_length=2048, blank=True, null=True)
    full_day = models.BooleanField(default=False, blank=True)
    timezone = TimeZoneField(default=settings.TIME_ZONE, blank=True, null=True)
    start_year = models.IntegerField()
    start_month = models.IntegerField()
    start_day = models.IntegerField()
    start_hour = models.IntegerField(blank=True, null=True)
    start_minute = models.IntegerField(blank=True, null=True)
    start_second = models.IntegerField(blank=True, null=True)
    end_year = models.IntegerField()
    end_month = models.IntegerField()
    end_day = models.IntegerField()
    end_hour = models.IntegerField(blank=True, null=True)
    end_minute = models.IntegerField(blank=True, null=True)
    end_second = models.IntegerField(blank=True, null=True)

    @property
    def start(self):
        """
        :return: Always returns a sensible time for ease of use
        """
        if self.full_day:
            return datetime.date(self.start_year,
                              self.start_month,
                              self.start_day)
        else:
            d = datetime.datetime(self.start_year,
                                  self.start_month,
                                  self.start_day,
                                  self.start_hour,
                                  self.start_minute,
                                  self.start_second)
            tz = self.timezone
        return tz.localize(d)

    @start.setter
    def start(self, value):
        """
        Always sets a sensible time for ease of use
        """
        self.start_year = value.year
        self.start_month = value.month
        self.start_day = value.day
        if not self.full_day:
            self.start_hour = value.hour
            self.start_minute = value.minute
            self.start_second = value.second
            if value.tzinfo is not None:
                self.timezone = value.tzinfo

    @property
    def end(self):
        """
        :return: Always returns a sensible time for ease of use
        """
        if self.full_day:
            return datetime.date(self.end_year,
                              self.end_month,
                              self.end_day)
        else:
            d = datetime.datetime(self.end_year,
                                  self.end_month,
                                  self.end_day,
                                  self.end_hour,
                                  self.end_minute,
                                  self.end_second)
            tz = self.timezone
        return tz.localize(d)

    @end.setter
    def end(self, value):
        """
        Always sets a sensible time for ease of use
        """
        self.end_year = value.year
        self.end_month = value.month
        self.end_day = value.day
        if not self.full_day:
            self.end_hour = value.hour
            self.end_minute = value.minute
            self.end_second = value.second
            if value.tzinfo is not None:
                self.timezone = value.tzinfo


class CalendarShare(models.Model):
    """
    A model allowing access to a calendar.
    """
    calendar = models.ForeignKey(Calendar, related_name='shares', editable=False)
    user = models.ForeignKey(CalendarUser)
    level = models.CharField(max_length=16, choices=ACCESS_LEVELS)


class EventInvite(models.Model):
    """
    A model signifying an invite to an event.
    """
    event = models.ForeignKey(Event, related_name='invites', editable=False)
    user = models.ForeignKey(CalendarUser, related_name='invites')
    state = models.CharField(max_length=16, choices=INVITE_STATES)
    user_event = models.OneToOneField(Event, null=True, related_name='parent_event', editable=False)
