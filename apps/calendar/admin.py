# -*- coding: utf-8 -*-
# Author Szymon Nowicki <szymon.nowicki@agitive.com>
# (C) Agitive sp. z o. o. 2015
# created at: 08/10/15 21:02

from __future__ import unicode_literals

from django.contrib import admin

from apps.calendar.models import Calendar, CalendarShare, Event, EventInvite


class CalendarShareInline(admin.TabularInline):
    model = CalendarShare


class CalendarAdmin(admin.ModelAdmin):
    inlines = [
        CalendarShareInline,
    ]


class EventInviteInline(admin.TabularInline):
    model = EventInvite
    fk_name = 'event'


class EventAdmin(admin.ModelAdmin):
    inlines = [
        EventInviteInline,
    ]


admin.site.register(Calendar, CalendarAdmin)
admin.site.register(Event, EventAdmin)
