# Sharelendar

## About

*Author: Szymon Nowicki*

This project was created as a recruitment assignment.

## Design

### Model

The most important element of the model is the Event.
It has to support both full day events, and events at a point in time.
This is achieved by mimicking the behavior of Python date and datetime objects.
The Event model has a field for year, month, day, hour, minute, and second,
of which the last three are onlu used for normal events.
Additionally it has a timezone field, used in normal events and a boolean deciding
if this event is full day or not.
This design ensures that a normal event can be painlessly turned into a full day one,
and a full day one can be turned into a normal event if supplied with the additional
required data.
